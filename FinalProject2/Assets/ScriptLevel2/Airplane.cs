﻿using System.Collections;
using System.Collections.Generic;
using ScriptManager;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Airplane : BaseEnemy
{
    [SerializeField] private float enginePower;
    [SerializeField] private float liftBooter;
    [SerializeField] private float drag;
    [SerializeField] private float angularDrag;
    [SerializeField] private float torque;
    private float terrainHight ;
    private Rigidbody  rb;

    public Rigidbody bulletAirplane;
    public Transform gun;

    // Start is called before the first frame update
    void Start()
    {

        rb = GetComponent<Rigidbody>();
        terrainHight = Terrain.activeTerrain.SampleHeight(transform.position);
    }


    void Update()
    {
       Fire();
    }

    public void Fire()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            Rigidbody bulletAirplaneInstantiate;
            bulletAirplaneInstantiate = Instantiate(bulletAirplane, gun.position, Quaternion.identity) as Rigidbody;
            bulletAirplaneInstantiate.velocity = (gun.forward * 1000f);
            bulletAirplaneInstantiate.drag = 0;
            SoundManager.Instance.Play(audioSource, SoundManager.Sound.JetFire);
            

        }
    }

      // Update is called once per frame
    void FixedUpdate()
    {   
        if (Input.GetKey(KeyCode.Space))
        {
            rb.AddForce(transform.forward * enginePower*Time.deltaTime);
        }
        Vector3 lift = Vector3.Project(rb.velocity,transform.forward);
        rb.AddForce(transform.up * lift.magnitude * liftBooter * Time.deltaTime);

        rb.drag = rb.velocity.magnitude * drag*Time.deltaTime;
        rb.angularDrag = rb.velocity.magnitude * angularDrag * Time.deltaTime ;

        transform.Rotate(Input.GetAxis("Vertical"), 0.0f, -1.0f*Input.GetAxis("Horizontal")*torque* Time.deltaTime);

        if (terrainHight > transform.position.y)
        {
            transform.position = new Vector3(transform.position.x, terrainHight, transform.position.z);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        
           if (other.tag == "Enermy")
            {
            SceneManager.LoadScene("GameoverLevel2");
            }
    }
}
