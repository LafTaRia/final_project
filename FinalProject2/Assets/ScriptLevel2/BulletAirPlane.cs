﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletAirPlane : MonoBehaviour
{
    [SerializeField] private float bulletSpeed;
    [SerializeField] private float interval;



    public void OnCollisionStay(Collision Other)
    {
        Destroy(gameObject);

    }
    private void Update()
    {
       

        if (interval > 0)
        {
            interval -= Time.deltaTime;
        }
        else
        {
            enabled = false;
            Destroy(gameObject);
        }
    }
}
