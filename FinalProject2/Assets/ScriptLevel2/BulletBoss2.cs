﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletBoss2 : MonoBehaviour
{
    [SerializeField] private float bulletSpeed;
    [SerializeField] private float interval;



    public void OnCollisionStay(Collision Other)
    {
        Destroy(gameObject);

    }
    private void Update()
    {
        transform.Translate(Vector3.forward * Time.deltaTime * bulletSpeed);

        if (interval > 0)
        {
            interval -= Time.deltaTime;
        }
        else
        {
            enabled = false;
            Destroy(gameObject);
        }
    }
}
