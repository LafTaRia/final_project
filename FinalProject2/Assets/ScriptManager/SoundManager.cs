using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ScriptManager
{

    public class SoundManager : Singleton<SoundManager>
    {
        [SerializeField] private SoundClip[] soundClips;
        [SerializeField] private AudioSource audioSource;

        

        [Serializable]
        public struct SoundClip
        {
            public Sound Sound;
            public AudioClip AudioClip;
        }

        public enum Sound
        {
            ManuBGM,
            BGM,
            BGM2,
            TankFire,
            HelicopterFire,
            JetFire,
            BossFire,
            Heal,
            PlayerDie,
            
        }

        public void Play(AudioSource audioSource, Sound sound)
        {
            Debug.Assert(audioSource != null, "audioSource cannot be null");
            audioSource.clip = GetAudioClip(sound);
            audioSource.Play();
        }
        
        
        public void PlayManuBGM()
        {
            audioSource.loop = true;
            Play(audioSource, Sound.ManuBGM);
        }


        public void PlayBGM()
        {
            audioSource.loop = true;
            Play(audioSource, Sound.BGM);
        }
        
        public void PlayBGM2()
        {
            audioSource.loop = true;
            Play(audioSource, Sound.BGM2);
        }
        
        

        private AudioClip GetAudioClip(Sound sound)
        {
            foreach (var soundClip in soundClips)
            {

                if (soundClip.Sound == sound)
                {
                    return soundClip.AudioClip;
                }

            }

            Debug.Assert(false, $"Cannot find sound {sound}");
            return null;
        }

        private void Start()
        {
            Debug.Assert(audioSource != null, "audioSource cannot be null");
            Debug.Assert(soundClips != null,"soundClips cannot be null");
            PlayManuBGM();
            
        }
    }
}

