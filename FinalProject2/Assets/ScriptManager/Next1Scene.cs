using System.Collections;
using System.Collections.Generic;
using ScriptManager;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Next1Scene : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            SceneManager.LoadScene("Level2");
            SoundManager.Instance.PlayBGM2();
                
        }
    }
}
