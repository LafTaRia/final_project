﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.SceneManagement;
public class HomeSence : MonoBehaviour
{
    [SerializeField] private Button HomeButton;


    public void Start()
    {
       HomeButton.onClick.AddListener(OnHomeButtonClicked);

    }

    private void OnHomeButtonClicked()
    {
        HomeButton.gameObject.SetActive(false);
        SceneManager.LoadScene("GameStart");
        ScoreManager.Instance.ClearFinalScore();
    }
}