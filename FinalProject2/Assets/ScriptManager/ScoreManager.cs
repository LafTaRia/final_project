using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : Singleton<ScoreManager>
{
    [SerializeField] private Text scoreText;
    [SerializeField] private Text finalScore;
    
    [SerializeField] private EnermyV1 enermyV1;
    private EnermyV1 enemyV1;
    
    [SerializeField] private EnermyV2 enermyV2;
    private EnermyV1 enemyV2;
    
    private int score;
    private int resultScore;


    


    public void SetScore(int score)
    {
        resultScore += score;
        scoreText.text = $"Score : {resultScore}";
        
    }


    public void GameClear()
    {
        scoreText.gameObject.SetActive(false);
        finalScore.gameObject.SetActive(true);
        finalScore.text = $"Your High Score : {resultScore}";
    }

    public void ShowScore()
    {
        scoreText.gameObject.SetActive(true);
    }

    public void GameOver1()
    {
        scoreText.gameObject.SetActive(false);
        
        
    }
    public void ClearFinalScore()   
    {
        finalScore.gameObject.SetActive(false);
    }

    
    
}
