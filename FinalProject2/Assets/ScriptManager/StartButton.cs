using System;
using System.Collections;
using System.Collections.Generic;
using ScriptManager;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class StartButton : MonoBehaviour
{
    [SerializeField] private Button startButton;
   

    public void Start()
    {
        startButton.onClick.AddListener(OnStartButtonClicked);
       
    }

    private void OnStartButtonClicked()
    {
        startButton.gameObject.SetActive(false);
        SceneManager.LoadScene("Level1");
        SoundManager.Instance.PlayBGM();
    }
    
    
    
}
