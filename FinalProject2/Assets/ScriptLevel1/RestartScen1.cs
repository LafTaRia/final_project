using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class RestartScen1 : MonoBehaviour
{
    [SerializeField] private Button restartButton1;
    public void Start()
    {
        restartButton1.onClick.AddListener(OnRestartButton1Clicked);
       
    }

    private void OnRestartButton1Clicked()
    {
        SceneManager.LoadScene("Level1");
        
        ScoreManager.Instance.ShowScore();
            
        
    }
}
