﻿using System.Collections;
using System.Collections.Generic;
using ScriptManager;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    [SerializeField] private Rigidbody bulletPrefabseris;
    [SerializeField] private GameObject targetPos;
    [SerializeField] private LayerMask layer; 
    [SerializeField] protected AudioSource audioSource;
    [SerializeField] Transform shootPoint;
    public float bulletforce;

    private Camera cameraTank;
    // Start is called before the first frame update
    void Start()
    {
        cameraTank = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {
        LaunchProjectile();
    }

    void LaunchProjectile()
    {
        Ray cameraRay = cameraTank.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(cameraRay, out hit, 100f, layer))
        {
            targetPos.SetActive(true);
            targetPos.transform.position = hit.point + Vector3.up * 0.03f;

            Vector3 Vo = CalculateProjectile(hit.point, shootPoint.position, 1f);

            transform.rotation = Quaternion.LookRotation(Vo);

            if (Input.GetMouseButtonDown(0))
            {
                Rigidbody fire = Instantiate(bulletPrefabseris, shootPoint.position, Quaternion.identity);
                fire.velocity = Vo*bulletforce*Time.deltaTime;
                FireSound();
                
            }
        }
            
        else
        {
            targetPos.SetActive(false);
        }

        Vector3 CalculateProjectile(Vector3 target, Vector3 origin, float time)
        {
            Vector3 distance = target - origin;
            Vector3 distanceXZ_plane = distance;
            distanceXZ_plane.y = 0f;

            float distanceY = distance.y;
            float distanceXZ = distanceXZ_plane.magnitude;

            float Vxz = distanceXZ / time;
            float Vy = distanceY / time + 0.1f * Mathf.Abs(Physics.gravity.y) * time;

            Vector3 result = distanceXZ_plane.normalized;
            result *= Vxz;
            result.y = Vy;

            return result;


        }

    }

    private void FireSound()
    {
        SoundManager.Instance.Play(audioSource,SoundManager.Sound.TankFire);
    }
}