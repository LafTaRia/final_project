﻿using System.Collections;
using System.Collections.Generic;
using ScriptManager;
using UnityEngine;
using UnityEngine.UI;

public class EnermyV2 : BaseEnemy
{

    
    [SerializeField] private float health;
    [SerializeField] private float maxHealth;
    [SerializeField] private GameObject bulletEnermy;
    [SerializeField] private float cooldownTime;
    private float currentShooting;
    public GameObject bulletSpawPoint;

    public GameObject healthBarUI;
    public Slider slider;

    public float runKill;

    void Start()
    {
        health = maxHealth;
        slider.value = CalculateHealth();
    }
    private void Update()
    {
        TakeHit();
    }

    public override void TakeHit()
    {
        currentShooting += Time.deltaTime;
        slider.value = CalculateHealth();

        if (health < maxHealth)
        {

            healthBarUI.SetActive(true);
        }

        if (health <= 0)
        {
            gameObject.SetActive(false);
            GameManager.Instance.OnEnemy2Destroyed();
        }

        if (health > maxHealth)
        {
            health = maxHealth;
        }
        
    }

    public void FixedUpdate()
    {
        transform.LookAt(player);

        float dist = Vector3.Distance(player.position, transform.position);

        if (dist < runKill)
        {
            if (currentShooting > cooldownTime)
            {
                Transform _bulletEnermy = Instantiate(bulletEnermy.transform, bulletSpawPoint.transform.position, Quaternion.identity);
                _bulletEnermy.transform.rotation = bulletSpawPoint.transform.rotation;
                currentShooting = 0;
                SoundManager.Instance.Play(audioSource,SoundManager.Sound.HelicopterFire);
            }
            
        }

    }

    public float CalculateHealth()
    {
        return health / maxHealth;
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Bullet")
        {
            health--;
        }
    }
}

