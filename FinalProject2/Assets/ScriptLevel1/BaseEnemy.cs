using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BaseEnemy : MonoBehaviour
{
    [SerializeField] protected Transform player;
    [SerializeField] protected AudioSource audioSource;


    public virtual void TakeHit()
    {
        
    }
}
