﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnermyV1 : BaseEnemy
{

    
    [SerializeField] private float movespeed;
    [SerializeField] private float health;
    [SerializeField] private float maxHealth;

    public GameObject healthBarUI;
    public Slider slider;

    public float runKill;

    void Start()
    {

        health = maxHealth;
        slider.value = CalculateHealth();
    }

    public void FixedUpdate()
    {
        transform.LookAt(player);

        float dist = Vector3.Distance(player.position, transform.position);

        if (dist < runKill)
        {
            transform.Translate(Vector3.forward * Time.deltaTime * movespeed);
        }

    }

   void Update()
    {
        TakeHit();
        
    }

   public override void  TakeHit()
   {
       slider.value = CalculateHealth();

       if (health < maxHealth)
       {

           healthBarUI.SetActive(true);
       }

       if (health <= 0)
       {
           gameObject.SetActive(false);
            
           GameManager.Instance.OnEnemy1Destroyed();
       }

       if (health > maxHealth)
       {
           health = maxHealth;
       }
   }

    public float CalculateHealth()
    {
        return health / maxHealth;
    }
    private void OnTriggerEnter(Collider other)
    {
       if(other.tag == "Bullet")
        {
            health--;
        }
    }

    
     
}
