﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletPlayer : MonoBehaviour
{

    [SerializeField] private float interval;
    private float healthEnermy;
    
    void Start()
    {
        /*GameObject.FindGameObjectWithTag("Enermy").layer;*/
    }
   
    public void OnCollisionStay(Collision Other)
    {
        Destroy(gameObject);

    }


    private void Update()
    {

        if(interval > 0)
        {
            interval -= Time.deltaTime;
        }
        else
        {
            enabled = false;
            Destroy(gameObject);
        }
    }

}
