﻿using System.Collections;
using System.Collections.Generic;
using ScriptManager;
using UnityEngine;

public class Heal : MonoBehaviour
{
    PlayerControllerscripts playerheal;
    [SerializeField] private float torque;
    [SerializeField] private float healboot;
    [SerializeField] protected AudioSource audioSource;
    private Rigidbody rb;
    
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        playerheal = FindObjectOfType<PlayerControllerscripts>();
    }

    // Update is called once per frame
    void Update()
    {
        rb.AddTorque(0, torque, 0 * Time.deltaTime ); 
    }

    private void OnTriggerEnter(Collider Other)
    {
        if(playerheal.health < playerheal.maxHealth)
        {
            
            /*Destroy(gameObject);*/
            gameObject.SetActive(false);
            playerheal.health = playerheal.health + healboot;
            
        }
    }
}
