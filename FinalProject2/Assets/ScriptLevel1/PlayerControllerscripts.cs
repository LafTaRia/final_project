﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerControllerscripts : MonoBehaviour
{
    

    CharacterController Canon;
    public float maxHealth;   
    public GameObject healthBarUI;
    public Slider slider;

    public float speed = 10;
    public float jumpspeed = 10;
    public float gravity = -9.81f;
    public float health;
    public Vector3 move;
    public Vector3 velocity;
    // Start is called before the first frame update
    void Start()
    {
        
        Canon = GetComponent<CharacterController>();

        health = maxHealth;
        slider.value = CalculateHealth();
    }

    // Update is called once per frame
    void Update()
    {
        move = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));

        Canon.Move(move * speed * Time.deltaTime);

        if(move != Vector3.zero)
        {
            transform.forward = move;
        }

        velocity.y += gravity   * Time.deltaTime;

        Canon.Move(velocity * Time.deltaTime);

        if(Canon.isGrounded && velocity.y < 0)
        {
            velocity.y = 0.0f;
        }

        if(Input.GetButtonDown("Jump") && Canon.isGrounded)
        {
            velocity.y += jumpspeed;
        }

        slider.value = CalculateHealth();

        if (health < maxHealth)
        {

            healthBarUI.SetActive(true);
        }

        if (health <= 0)
        {
            
            SceneManager.LoadScene("GameoverLevel1");
            ScoreManager.Instance.GameOver1();
            ScoreManager.Instance.SetScore(0);
            
        }

        if (health > maxHealth)
        {
            health = maxHealth;
        }
        if (transform.position.y < -10)
        {
            SceneManager.LoadScene("GameoverLevel1");
            ScoreManager.Instance.GameOver1();
            ScoreManager.Instance.SetScore(0);
        }
    }

    public float CalculateHealth()
    {
        return health / maxHealth;
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Enermy")
        {
            health--;
        }
    }

}
