using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class RestartScene2 : MonoBehaviour

{
    [SerializeField] private Button restartButton;
    public void Start()
    {
        restartButton.onClick.AddListener(OnRestartButtonClicked);
       
    }

    private void OnRestartButtonClicked()
    {
        SceneManager.LoadScene("Level2");
        ScoreManager.Instance.ShowScore();
            
        
    }

}
